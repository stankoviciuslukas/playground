terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.9.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.34.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

variable "do_token" {}

provider "aws" {
  region = "eu-central-1"
}

# Use S3 as backend
terraform {
  backend "s3" {
    bucket = "playground-state-bucket"
    region = "eu-central-1"
    key    = "terraform.tfstate"
  }
}
