resource "digitalocean_kubernetes_cluster" "playground" {
  name   = "playground"
  region = "fra1"
  version = "1.21.3-do.0"
  auto_upgrade = true

  node_pool {
    name       = "worker-pool-new"
    size       = "s-2vcpu-2gb"
    node_count = 3

  }
}
