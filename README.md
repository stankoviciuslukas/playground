# playground

This repository is dedicated to play with technologies in a sandbox environment (mostly k8s).

## kubernetes

To deploy sandbox env run gitlab pipeline which will deploy kubernetes cluster on DO.

Note: DO was choosen as a Cloud Provider mostly because of the pricing.

## Linkerd

## Hashi Vault

echo "${serverCert}" | openssl base64 -d -A -out ./vault.crt
kubectl config view --raw --minify --flatten -o jsonpath='{.clusters[].cluster.certificate-authority-data}' | base64 -d > ./vault.ca

k create secret generic vault-tls-secret --from-file=vault.key=./vault.key --from-file=vault.ca=./vault.ca --from-file=vault.crt=./vault.crt

## ArgoCD

Located in `argocd` folder there is an `app` folder and a `application.yaml`

ArgoCD configuration in defined in application.yaml. It specifies what git repository to sync with and which path on that directory to monitor.

When ArgoCD will detected that the destined cluster is not in sync with repos configuration it will automatically deploy newest changes on specified internal and/or external cluster.

## Argo Workflows



## 

1. Deploy ArgoCD
2. Configure an Application manifest
3. Use Helm Charts
4. Simulate a demo

Install gcloud